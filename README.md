The PRS2 coding standard with gsjlint.
===========================

Please have a look to this [example](http://closuretools.blogspot.co.uk/2010/08/introducing-closure-linter.html)
and [overview](https://developers.google.com/closure/utilities/).
I found it handy and wrote a small script to install gsjlint and integrate it with php code sniffer. 

Installation:

    git clone git@bitbucket.org:amadu80/phpcs-ninja.git 
    cd phpcs-ninja
    sudo ./install.sh
    
Use:

    phpcs -p --standard=Ninja <path>

Cheers!
