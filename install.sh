#!/bin/bash
sudo apt-get install python-setuptools
sudo easy_install http://closure-linter.googlecode.com/files/closure_linter-latest.tar.gz
sudo phpcs --config-set gjslint_path `which gjslint`
sudo mkdir /usr/share/php/PHP/CodeSniffer/Standards/Ninja
cp ruleset.xml /usr/share/php/PHP/CodeSniffer/Standards/Ninja/ruleset.xml 
